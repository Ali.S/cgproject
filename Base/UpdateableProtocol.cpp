#include "pcHeaders.h"
#include "UpdateableProtocol.h"
#include "Scheduler.h"

UpdateableProtocol::UpdateableProtocol()
{

}

UpdateableProtocol::~UpdateableProtocol()
{
	
}

void UpdateableProtocol::startUpdating()
{
	if (inUpdateList)
		return;
	Scheduler::sharedScheduler()->addTarget(this);	
	inUpdateList = true;
}

void UpdateableProtocol::stopUpdating()
{
	if (!inUpdateList)
		return;
	Scheduler::sharedScheduler()->removeTarget(this);
	inUpdateList = false;
}
