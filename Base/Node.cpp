#include "pcHeaders.h"
#include "Node.h"


Node::Node()
{
}


Node::~Node()
{
	for(auto child:children_)
	{
		child->parent_ = nullptr;
		dynamic_cast<Object*>(child)->release();
	}
}

void Node::preRender()
{
	RenderableProtocol::preRender();
	for(unsigned i=0;i<children_.size();i++)
		if (dynamic_cast<RenderableProtocol*> (children_[i]))
			dynamic_cast<RenderableProtocol*> (children_[i])->preRender();
}

void Node::render()
{
	if(!getVisible())
		return;
	for(unsigned i=0;i<children_.size();i++)
		if (dynamic_cast<RenderableProtocol*> (children_[i]))
			dynamic_cast<RenderableProtocol*> (children_[i])->render();
}

void Node::postRender()
{
	for(unsigned i=0;i<children_.size();i++)
		if (dynamic_cast<RenderableProtocol*> (children_[i]))
			dynamic_cast<RenderableProtocol*> (children_[i])->postRender();
}

void Node::addChild(Node::ChildArrayType::value_type child)
{
	dynamic_cast<Object*>(child)->retain();
	child->parent_ = this;
	children_.push_back(child);
}

void Node::removeChild(Node::ChildArrayType::iterator child)
{	
	dynamic_cast<Object*>(*child)->release();
	(*child)->parent_ = nullptr;
	children_.erase(child);	
}

const Node::ChildArrayType& Node::getChildren()
{
	return children_;
}

Node::ChildArrayType::iterator Node::childrenBegin()
{
	return children_.begin();
}

Node::ChildArrayType::iterator Node::childrenEnd()
{
	return children_.end();
}

Node::ChildArrayType::const_iterator Node::childrenBegin() const
{
	return children_.begin();
}

Node::ChildArrayType::const_iterator Node::childrenEnd() const
{
	return children_.end();
}