#pragma once

extern const char* ShaderPositionitionUniformColor_v;
extern const char* ShaderPositionitionUniformColor_f;

extern const char* ShaderPositionitionColorUniformColor_v;
extern const char* ShaderPositionitionColorUniformColor_f;

extern const char* ShaderPositionitionTextureUniformColor_v;
extern const char* ShaderPositionitionTextureUniformColor_f;

extern const char* ShaderPositionitionTextureColorUniformColor_v;
extern const char* ShaderPositionitionTextureColorUniformColor_f;

extern const char* ShaderPositionition_v;
extern const char* ShaderPositionition_f;

extern const char* ShaderPositionitionColor_v;
extern const char* ShaderPositionitionColor_f;

extern const char* ShaderPositionitionTexture_v;
extern const char* ShaderPositionitionTexture_f;

extern const char* ShaderPositionitionTextureColor_v;
extern const char* ShaderPositionitionTextureColor_f;
