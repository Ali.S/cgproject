#pragma once

#include "Object.h"
#include "PositionProtocol.h"

class Camera : public Object, public PositionProtocol
{
public: 
	Camera();
	~Camera();
	void setPerspective(float fovY, float aspect, float near, float far);
	void setOrthogonal(float aspect, float near, float far);
	void setVeiw(const kmVec3& eye, const kmVec3& center, const kmVec3& up);
	const kmMat4& getProjectionViewMatrix();
	const kmMat4 getViewMatrix();
	const kmMat4& getProjectionMatrix();
	static Camera* getActiveCamera();
	void setAsActive();

private:
	static Camera* activeCamera_;
	kmMat4 projectionVeiwMatrix_;
	kmMat4 projectionMatrix_;
	kmMat4 veiwMatrix_;	
};