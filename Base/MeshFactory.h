#pragma once
#include "Object.h"

class Mesh;

class MeshFactory : public Object
{
public:
	MeshFactory();
	Mesh* createSphere(unsigned vSlice, unsigned hSlice);
	Mesh* assignSphere(Mesh* target, unsigned vSlice, unsigned hSlice);
};