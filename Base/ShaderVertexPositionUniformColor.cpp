#include "pcHeaders.h"
#include "DefaultShaders.h"

#define STRINGIFY(X) #X

const char* ShaderPositionitionUniformColor_v = STRINGIFY(
	#version 120									\n
													\n
	uniform mat4 u_Pmat;							\n
	uniform mat4 u_Vmat;							\n
	uniform mat4 u_Mmat;							\n
	attribute vec4 a_Position;						\n	
													\n
	void main()										\n
	{												\n
		gl_Position = u_Pmat * u_Vmat * u_Mmat * a_Position;		\n

	}
	);

const char* ShaderPositionitionUniformColor_f = STRINGIFY(
	#version 120									\n
													\n
	uniform vec4 u_Color;							\n
													\n
	void main()										\n
	{												\n
		gl_FragColor = u_Color;						\n
	}
	);