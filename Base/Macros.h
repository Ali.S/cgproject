#define PROPERTY(Type, Name, FunName)\
	private:\
	Type Name;\
	public:\
		Type get##FunName();\
		void set##FunName(Type FunName);
	