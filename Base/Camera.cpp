#include "pcHeaders.h"
#include "Camera.h"

Camera* Camera::activeCamera_ = nullptr;

Camera::Camera()
{
	setOrthogonal(1,1e-2, 1e2);
	kmVec3 eye;
	eye.x = 0; eye.y = 0; eye.z = 1;
	kmVec3 center;
	center.x = 0; center.y = 0; center.z = 0;
	kmVec3 up;
	up.x = 0; up.y = 1; up.z = 0;
	setVeiw(eye,center,up);
	kmMat4Multiply(&projectionVeiwMatrix_,&projectionMatrix_, &veiwMatrix_);
}

Camera::~Camera()
{
	if (activeCamera_ == this)
		activeCamera_ = nullptr;
}

void Camera::setOrthogonal(float aspect, float near, float far)
{
	kmMat4OrthographicProjection(&projectionMatrix_, -aspect, aspect, -1, 1, near, far);
}

void Camera::setVeiw(const kmVec3& eye, const kmVec3& center, const kmVec3& up)
{
	kmMat4LookAt(&veiwMatrix_, &eye, &center, &up);
}

void Camera::setPerspective(float fovY, float aspect, float near, float far)
{
	kmMat4PerspectiveProjection(&projectionMatrix_, fovY, aspect, near, far);	
}

const kmMat4& Camera::getProjectionViewMatrix()
{
	Camera* temp = activeCamera_;
	activeCamera_ = nullptr;
	recalculateWorldTransformationMatrix(true);
	activeCamera_ = temp;
	
	kmMat4Inverse(&projectionVeiwMatrix_, &getWorldTransformationMatrix());
	
	kmMat4Multiply(&projectionVeiwMatrix_, &veiwMatrix_, &projectionVeiwMatrix_);
	kmMat4Multiply(&projectionVeiwMatrix_, &projectionMatrix_, &projectionVeiwMatrix_);
	return projectionVeiwMatrix_;
}

const kmMat4 Camera::getViewMatrix()
{
	Camera* temp = activeCamera_;
	activeCamera_ = nullptr;
	recalculateWorldTransformationMatrix(true);
	activeCamera_ = temp;
	kmMat4 res;
	
	kmMat4Inverse(&res, &getWorldTransformationMatrix());
	
	kmMat4Multiply(&res, &veiwMatrix_, &res);	
	return res;
}

const kmMat4& Camera::getProjectionMatrix()
{
	return projectionMatrix_;
}

void Camera::setAsActive()
{
	activeCamera_ = this;
}

Camera* Camera::getActiveCamera()
{
	return activeCamera_;
}