#pragma once
#include "Shader.h"
#include <memory>
#include <string>
#include <unordered_map>

#define SHADERNAME_POSITION_U_COLOR "SH_PUC"
#define SHADERNAME_POSITION_TEXTURE_U_COLOR "SH_PTUC"
#define SHADERNAME_POSITION_COLOR_U_COLOR "SH_PCUC"
#define SHADERNAME_POSITION_TEXTURE_COLOR_U_COLOR "SH_PTCUC"
#define SHADERNAME_POSITION "SH_PUW"
#define SHADERNAME_POSITION_TEXTURE "SH_PTUW"
#define SHADERNAME_POSITION_COLOR "SH_PCUW"
#define SHADERNAME_POSITION_TEXTURE_COLOR "SH_PTCUW"


class ShaderCache
{
public: 
	ShaderCache();
	static ShaderCache* sharedShaderCache();
	static void purgeSharedShaderCache();
	void addShader(std::string name, Shader* shader);
	void removeShader(std::string name);
	Shader* getShader(std::string name);
private:
	void loadDefaultShaders();
	std::unordered_map<std::string, Shader*> shaderLibrary_;
	static ShaderCache* sharedShaderCache_;
};