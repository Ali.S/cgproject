#pragma once
#include "Object.h"
#include "Macros.h"
#include "RenderableProtocol.h"
#include <gl/GL.h>
#include "Shader.h"
#include <memory>
#include "GLGlobals.h"
#include <vector>

class Texture2D;

class Mesh : public Object, public RenderableProtocol
{
public:
	struct VertexInfo
	{
		union{
			struct {
				float u,v;
			};
			struct {
				float u,v;
			} textureCoordination;
		};
		union {
			struct {
				float r,g,b,a;
			};
			struct {
				float r,g,b,a;
			} color;
		};
		union{
			struct{
				float nx,ny,nz;
			};
			struct {
				float x,y,z;
			} normal;
		};
		union {
			struct {
				float x,y,z;
			};
			struct {
				float x,y,z;
			}position;
		};		
	};

	Mesh();
	Mesh(const Mesh&);
	Mesh(const VertexInfo* vertices, int vertexCount);
	Mesh(const VertexInfo* vertices, int vertexCount, const short* indices, int indexCount);

	~Mesh();
	
	virtual void render();
	virtual void postRender();
	void setShader(Shader* shader);
	Shader* getShader();
	template<class T> void setData(const T* vertices, int vertexCount);
	template<class T> void setData(const T* vertices, int vertexCount, const short* indices, int indexCount);	
	GLuint getArrayBufferID();
	GLuint getElementArrayBufferID();
	virtual void load(const char* path);
	void setUniformColor(float r,float g,float b, float a = 1.0f);
	void disposeData();
	void removeUniformColor();
	
	PROPERTY(Texture*, texture_, Texture);
	PROPERTY(bool, usePerVertexColor_, UsePerVertexColor);
	PROPERTY(bool, autoSelectShader_, AutoSelectShader);
	PROPERTY(bool, hasNormals_, HasNormals);

protected:	
	bool useIndices_;	
	struct {
		float r,g,b,a;
		bool inUse;
	} uniformColor_;
	unsigned indicesCount_;	
	Shader* shader_;
	struct VBOData
	{
		GLuint vertices;
		GLuint indices;
	};	
	std::shared_ptr<VBOData> VBOID_;
	struct
	{
		int position,color,textureCoordinates,normals;		
	} vertexAttributeIDs_;
	struct
	{
		void* positionOffset_;
		void* colorOffset_;
		void* texCoordOffset_;
		void* normalOffset_;
	} vertexAttributeOffsets_;
	struct
	{
		int VMatrix, PMatrix, MMatrix, texture,color;
	} uniformIDs_;
	
	int vertexAttributeStride_;
	void objFileLoader(const char* path);
};

template<class T> void Mesh::setData(const T* vertices, int vertexCount, const short* indices, int indexCount)
{
	if (!VBOID_)
	{
		VBOID_ = std::make_shared<VBOData>();
		glGenBuffers(2, reinterpret_cast<GLuint*>(VBOID_.get()));		
	}	
	GLGlobals::sharedGLGlobals()->setArrayBuffer(VBOID_->vertices);
	GLGlobals::sharedGLGlobals()->setElementArrayBuffer(VBOID_->indices);
	glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(T), vertices, GL_STATIC_DRAW);	
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexCount * sizeof(short), indices, GL_STATIC_DRAW);
	useIndices_ = true;
	indicesCount_ = indexCount;
	vertexAttributeStride_ = sizeof(T);	
	vertexAttributeOffsets_.colorOffset_ = &(reinterpret_cast<T*>(0)->color);
	vertexAttributeOffsets_.positionOffset_= &(reinterpret_cast<T*>(0)->position);
	vertexAttributeOffsets_.normalOffset_ = &(reinterpret_cast<T*>(0)->normal);
	vertexAttributeOffsets_.texCoordOffset_= &(reinterpret_cast<T*>(0)->textureCoordination);
}

template<class T> void Mesh::setData(const T* vertices, int vertexCount)
{
	if (!VBOID_)
	{
		VBOID_ = std::make_shared<VBOData>(VBOData());
		glGenBuffers(2, reinterpret_cast<GLuint*>(VBOID_.get()));		
	}	
	GLGlobals::sharedGLGlobals()->setArrayBuffer(VBOID_->vertices);	
	glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(T), vertices, GL_STATIC_DRAW);
	useIndices_ = false;
	indicesCount_ = vertexCount;
	vertexAttributeStride_ = sizeof(T);	
	vertexAttributeOffsets_.colorOffset_ = &(reinterpret_cast<T*>(0)->color);
	vertexAttributeOffsets_.positionOffset_= &(reinterpret_cast<T*>(0)->position);
	vertexAttributeOffsets_.normalOffset_ = &(reinterpret_cast<T*>(0)->normal);
	vertexAttributeOffsets_.texCoordOffset_= &(reinterpret_cast<T*>(0)->textureCoordination);
}
