#include "pcHeaders.h"
#include "Texture2D.h"
#include <IL/il.h>
#include <gl/GL.h>
#include <iostream>
#include "GLGlobals.h"

Texture2D::Texture2D(const char* fileName)
{
	load(fileName);
	GLGlobals::sharedGLGlobals()->bindTexture(this);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	minifyFilter_ = magnifyFilter_ = GL_LINEAR;
	wrapModeS_ = wrapModeT_ = GL_CLAMP_TO_EDGE;
}

Texture2D::Texture2D()
{
	GLGlobals::sharedGLGlobals()->bindTexture(this);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	minifyFilter_ = magnifyFilter_ = GL_LINEAR;
	wrapModeS_ = wrapModeT_ = GL_CLAMP_TO_EDGE;
}

Texture2D::~Texture2D()
{	
}

void Texture2D::setWrapMode(GLenum wrapModeS, GLenum wrapModeT)
{
	if(wrapModeS != wrapModeS_ || wrapModeT != wrapModeT_)
		GLGlobals::sharedGLGlobals()->bindTexture(this);
	if(wrapModeS != wrapModeS_)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapModeS);
	if(wrapModeT != wrapModeT_)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapModeT);
	wrapModeS_ = wrapModeS;
	wrapModeT_ = wrapModeT;
}

void Texture2D::setScaleFilter(GLenum minifyFilter, GLenum magnifyFilter)
{
	if(minifyFilter != minifyFilter_ || magnifyFilter != magnifyFilter_)
		GLGlobals::sharedGLGlobals()->bindTexture(this);
	if(minifyFilter != minifyFilter_)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minifyFilter);
	if(magnifyFilter != magnifyFilter_)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magnifyFilter);
	minifyFilter_ = minifyFilter;
	magnifyFilter_ = magnifyFilter_;
}

unsigned int Texture2D::getWidth()
{
	return width_;
}

unsigned int Texture2D::getHeight()
{
	return height_;
}

bool Texture2D::load(const char* fileName)
{
	int imgNum = ilGenImage();
	ilBindImage(imgNum);
	
	while(true)
	{
		if(!ilLoadImage(fileName))
		{
			std::cerr << "error loading image " << fileName << "\n";
			std::cerr << ilGetError() << "\n";
			break;
		}

		width_ = ilGetInteger(IL_IMAGE_WIDTH);
		height_ = ilGetInteger(IL_IMAGE_HEIGHT);
		int Bpp = ilGetInteger(IL_IMAGE_BPP);
		int format = ilGetInteger(IL_IMAGE_FORMAT);
		GLchar* data = reinterpret_cast<GLchar*>(ilGetData());
		bool success = true;
		switch (Bpp)
		{
		case 1:{
			switch (format)
			{
			case IL_LUMINANCE: success = bufferData(GL_LUMINANCE, width_, height_, data); break;
			default: success = false; break;
			}	
			break;
		}
		case 3:
		{
			switch (format)
			{
			case IL_RGB: success = bufferData(GL_RGB, width_, height_, data);break;
			case IL_BGR: success = bufferData(GL_BGR, width_, height_, data);break;
			default: success = false; break;
			}	
			break;
		}
		case 4:
		{
			switch (format)
			{
			case IL_RGBA: success = bufferData(GL_RGBA, width_, height_, data); break;
			case IL_BGRA: success = bufferData(GL_BGRA, width_, height_, data); break;
			default: success = false; break;
			}	
			break;
		}
		default: success = false; break;
		}
		if (!success)
			break;
		ilDeleteImage(imgNum);
		return true;
	}
	ilDeleteImage(imgNum);
	return false;
}

bool Texture2D::bufferData(int mode, unsigned width, unsigned height, void* data)
{
	GLGlobals::sharedGLGlobals()->bindTexture(this);
	switch (mode)
	{
		case GL_LUMINANCE: glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, data);break;
		case GL_RGB: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);break;
		case GL_BGR: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);break;
		case GL_RGBA: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);break;
		case GL_BGRA: glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, data);break;
		case GL_DEPTH_COMPONENT:glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, data);break;
		default: return false;
	}
	
	width_ = width;
	height_ = height;	
	return true;
}

GLenum Texture2D::getType()
{
	return GL_TEXTURE_2D;
}