#include "pcHeaders.h"
#include "TransformFeedbackShader.h"
#include <cstdlib>
#include <iostream>
#include "GLGlobals.h"

using namespace std;	

TransformFeedbackShader::TransformFeedbackShader()
{

}

TransformFeedbackShader::TransformFeedbackShader(const char* vertexShader, const char* geometryShader, std::vector<const char*> targets)
{
	compile(vertexShader, geometryShader, targets);
}

TransformFeedbackShader::TransformFeedbackShader(const char* vertexShader, std::vector<const char*>targets)
{
	compile(vertexShader, targets);
}


TransformFeedbackShader::~TransformFeedbackShader() 
{

}

bool TransformFeedbackShader::compile(const char* vertexShader, const char* geometryShader, std::vector<const char*> targets)
{
	if (programID_ != 0)
		glDeleteProgram(programID_);
	programID_ = glCreateProgram();
	if (!programID_)
	{
		std::cerr << "ERROR: unable to create TransformFeedbackShader program\n";
		return false;
	}
	
	GLuint vShaderID;
	GLuint gShaderID;
	GLint status = 0;
	
	status |= compileShader(vShaderID, GL_VERTEX_SHADER, vertexShader) ? 1 : 0;
	status |= compileShader(gShaderID, GL_GEOMETRY_SHADER, geometryShader) ? 2 :0;
	
	if (status != 3)
	{
		std::cerr << "\n";
		glDeleteProgram(programID_);
		if (status & 1)
			glDeleteShader(vShaderID);
		if (status & 2)
			glDeleteShader(gShaderID);
		programID_ = 0;
		return false;
	}
	
	glTransformFeedbackVaryings(programID_, targets.size(), targets.data(), GL_INTERLEAVED_ATTRIBS);
	
	glLinkProgram(programID_);	
	glGetProgramiv(programID_, GL_LINK_STATUS, &status);
	glDeleteShader(vShaderID);
	glDeleteShader(gShaderID);
	if (!status)
	{
		GLchar *src;
		std::cerr << "ERROR: linking TransformFeedbackShader failed.\n";
		src = logForOpenGLObject(programID_, glGetProgramiv, glGetProgramInfoLog);
        std::cerr << "ERROR: linker output\n" << src;
		free (src);
		programID_ = 0;	
		return false;
	}
	
	return true;
}

bool TransformFeedbackShader::compile(const char* vertexShader, std::vector<const char*> targets)
{
	if (programID_ != 0)
		glDeleteProgram(programID_);
	programID_ = glCreateProgram();
	if (!programID_)
	{
		std::cerr << "ERROR: unable to create TransformFeedbackShader program\n";
		return false;
	}
	
	GLuint vShaderID;	
	GLint status = 0;
	
	status |= compileShader(vShaderID, GL_VERTEX_SHADER, vertexShader) ? 1 : 0;	
	unsigned int a;
	compileShader(a,GL_FRAGMENT_SHADER,"#version 100\nvoid main(){gl_FragColor=vec4(1.0);}");
	
	if (status != 1)
	{
		std::cerr << "\n";
		glDeleteProgram(programID_);
		programID_ = 0;
		return false;
	}
	
	glTransformFeedbackVaryings(programID_, targets.size(), targets.data(), GL_INTERLEAVED_ATTRIBS);

	glLinkProgram(programID_);	
	glGetProgramiv(programID_, GL_LINK_STATUS, &status);
	glDeleteShader(vShaderID);
	if (!status)
	{
		GLchar *src;
		std::cerr << "ERROR: linking TransformFeedbackShader failed.\n";
		src = logForOpenGLObject(programID_, glGetProgramiv, glGetProgramInfoLog);
        std::cerr << "ERROR: linker output\n" << src;
		free (src);
		programID_ = 0;	
		return false;
	}
	
	return true;
}

unsigned int TransformFeedbackShader::compute(GLenum resultType, int primitiveCount, bool queryGeometryCount)
{
	if(!inUse())
		use();
	GLuint query;	
	int err = 0;
	unsigned int geometryCount = primitiveCount;
	glEnable(GL_RASTERIZER_DISCARD);
	err = glGetError();
	if(queryGeometryCount)
	{
		glGenQueries(1, &query);
		glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
	}
	err = glGetError();
	glBeginTransformFeedback(resultType);	
	err = glGetError();
	glDrawArrays(resultType, 0, primitiveCount);
	err = glGetError();
	glEndTransformFeedback();	
	err = glGetError();
	if(queryGeometryCount)
	{
		glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);	
		glGetQueryObjectuiv(query, GL_QUERY_RESULT, &geometryCount);
	}
	err = glGetError();	
	glDisable(GL_RASTERIZER_DISCARD);
	return geometryCount;
}

unsigned int TransformFeedbackShader::compute(GLenum resultType, int primitiveCount, GLenum primitiveType, bool queryGeometryCount)
{
	if(!inUse())
		use();
	GLuint query;	
	unsigned int geometryCount = primitiveCount;
	glEnable(GL_RASTERIZER_DISCARD);
	if(queryGeometryCount)
	{
		glGenQueries(1, &query);
		glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
	}
	glBeginTransformFeedback(resultType);	
	glDrawArrays(primitiveType, 0, primitiveCount);
	glEndTransformFeedback();	
	if(queryGeometryCount)
	{
		glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);	
		glGetQueryObjectuiv(query, GL_QUERY_RESULT, &geometryCount);
	}	
	glDisable(GL_RASTERIZER_DISCARD);
	return geometryCount;
}
