#include "pcHeaders.h"
#include "Scheduler.h"
#include "UpdateableProtocol.h"
#include "Object.h"
#include <algorithm>
#include <functional>

Scheduler* Scheduler::sharedScheduler_ = nullptr;

Scheduler::Scheduler() : listLocked_ (false)
{

}

Scheduler::~Scheduler() 
{
	for(auto i: updateList_)
		dynamic_cast<Object*>(i)->release();
}

Scheduler* Scheduler::sharedScheduler()
{
	if (sharedScheduler_ == nullptr)
		sharedScheduler_ = new Scheduler;
	return sharedScheduler_;
}

void Scheduler::purgeSceduler()
{
	delete sharedScheduler_;
	sharedScheduler_ = nullptr;
}

void Scheduler::step(float dt)
{
	if (listLocked_)
		return;
	listLocked_ = true;
	for(unsigned i=0;i<updateList_.size();i++)
		updateList_[i]->update(dt);
	std::make_heap(removeList_.begin(), removeList_.end());
	for(unsigned i = removeList_.front();removeList_.size(); i =removeList_.front())
	{
		dynamic_cast<Object*>(updateList_[i])->release();
		updateList_[i] = updateList_.back();
		updateList_.pop_back();
		std::pop_heap(removeList_.begin(), removeList_.end());
		removeList_.pop_back();
	}
	for(unsigned i=0;i<addList_.size();i++)
	{
		dynamic_cast<Object*>(updateList_[i])->retain();
		updateList_.push_back(addList_[i]);		
	}
	addList_.clear();
	listLocked_ = false;
}

void Scheduler::addTarget(UpdateableProtocol* target)
{
	if (listLocked_)
		addList_.push_back(target);
	else
	{
		dynamic_cast<Object*>(target)->retain();
		updateList_.push_back(target);
	}	
}

void Scheduler::removeTarget(UpdateableProtocol* target)
{
	unsigned targetID;
	for(targetID=0;targetID<updateList_.size();targetID++)
		if (updateList_[targetID] == target)
			break;
	
	if (listLocked_)
		removeList_.push_back(targetID);
	else
	{
		dynamic_cast<Object*>(updateList_[targetID])->release();
		updateList_[targetID] = updateList_.back();
		updateList_.pop_back();
	}
}