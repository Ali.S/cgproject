#pragma once
#include <GL/gl.h>
#include "ShaderBase.h"
#include "../KazMath/kazmath.h"
#include <vector>

class TransformFeedbackShader : public ShaderBase
{
public:
	TransformFeedbackShader();
	TransformFeedbackShader(const char* vertexShader, std::vector<const char*> targets);
	TransformFeedbackShader(const char* vertexShader, const char* geometryShader, std::vector<const char*> targets);
	~TransformFeedbackShader();
	bool compile(const char* vertexShader, std::vector<const char*> targets);
	bool compile(const char* vertexShader, const char* geometryShader, std::vector<const char*> targets);	
	unsigned int compute(GLenum resultType,int primitiveCount, bool queryGeometryCount = false);
	unsigned int compute(GLenum resultType,int primitiveCount, GLenum primiteType, bool queryGeometryCount = false);	
};