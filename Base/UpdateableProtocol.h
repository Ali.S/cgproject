#pragma once

class UpdateableProtocol
{
public :
	UpdateableProtocol();
	virtual ~UpdateableProtocol();
	virtual void update(float dt) = 0;
	void startUpdating();
	void stopUpdating();
private:
	bool inUpdateList;	
};