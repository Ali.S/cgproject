#include "pcHeaders.h"
#include "ShaderBase.h"
#include <iostream>

ShaderBase* ShaderBase::inUseShader_ = nullptr;

ShaderBase::ShaderBase() : programID_(0)
{
}

ShaderBase::~ShaderBase()
{
	if (programID_ != 0)
		glDeleteProgram(programID_);
}

bool ShaderBase::compileShader(GLuint& shaderID, GLenum type, const GLchar* source)
{
    GLint status;
	shaderID = 0;
    if (!source)    
        return false;
    

    shaderID = glCreateShader(type);
	if (shaderID == 0)
		return false;
    glShaderSource(shaderID, 1, &source, nullptr);
    glCompileShader(shaderID);

    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status);

    if (!status)
    {
        GLsizei length;
		glGetShaderiv(shaderID, GL_SHADER_SOURCE_LENGTH, &length);
		GLchar* src = (GLchar*)malloc(sizeof(GLchar) * length);
		
		glGetShaderSource(shaderID, length, NULL, src);
		std::cerr << "ERROR: Failed to compile shader:\n" << src << "\n";
		free(src);
        
        src = logForOpenGLObject(shaderID, glGetShaderiv, glGetShaderInfoLog);
        std::cerr << "ERROR: Compiler output\n" << src << "\n";
		free (src);
        
    }
	glAttachShader(programID_, shaderID);

    return (status == GL_TRUE);
}


char* ShaderBase::logForOpenGLObject(GLuint object, void(__stdcall *infoFunc)(GLuint, GLenum, GLint*), void(__stdcall *logFunc)(GLuint, GLsizei, GLsizei*, GLchar*))
{
    GLint logLength = 0, charsWritten = 0;

    infoFunc(object, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength < 1)
        return 0;

    char *logBytes = (char*)malloc(logLength);
    logFunc(object, logLength, &charsWritten, logBytes);
    

	return logBytes;
}

void ShaderBase::use()
{
	if(inUseShader_ != this)
	{
		glUseProgram(programID_);
		inUseShader_ = this;
	}
}

bool ShaderBase::inUse()
{
	return inUseShader_ == this;
}

bool ShaderBase::isUsable()
{
	return programID_ != 0;
}

ShaderBase* ShaderBase::getInUseShader()
{
	return inUseShader_;
}

int ShaderBase::getUniformID(const char* name)
{
	return glGetUniformLocation(programID_, name);
}

int ShaderBase::getAttribitueID(const char* name)
{
	return glGetAttribLocation(programID_, name);
}

void ShaderBase::setUniform(int ID, float v0)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniform1f(ID, v0);
		temp->use();
	}
	else
		glUniform1f(ID, v0);
}

void ShaderBase::setUniform(int ID, float v0, float v1)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniform2f(ID, v0, v1);
		temp->use();
	}
	else
		glUniform2f(ID, v0, v1);
}

void ShaderBase::setUniform(int ID, float v0, float v1, float v2)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniform3f(ID, v0, v1, v2);
		temp->use();
	}
	else
		glUniform3f(ID, v0, v1, v2);
}

void ShaderBase::setUniform(int ID, float v0, float v1, float v2, float v3)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniform4f(ID, v0, v1, v2, v3);
		temp->use();
	}
	else
		glUniform4f(ID, v0, v1, v2, v3);
}

void ShaderBase::setUniform(int ID, int v0)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniform1i(ID, v0);
		temp->use();
	}
	else
		glUniform1i(ID, v0);
}

void ShaderBase::setUniform(int ID, int v0, int v1)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniform2i(ID, v0, v1);
		temp->use();
	}
	else
		glUniform2i(ID, v0, v1);
}

void ShaderBase::setUniform(int ID, int v0, int v1, int v2)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniform3i(ID, v0, v1, v2);
		temp->use();
	}
	else
		glUniform3i(ID, v0, v1, v2);
}

void ShaderBase::setUniform(int ID, int v0, int v1, int v2, int v3)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniform4i(ID, v0, v1, v2, v3);
		temp->use();
	}
	else
		glUniform4i(ID, v0, v1, v2, v3);
}

void ShaderBase::setUniform(int ID, kmMat3 v0)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniformMatrix3fv(ID, 1, GL_FALSE, v0.mat);
		temp->use();
	}
	else
		glUniformMatrix3fv(ID, 1, GL_FALSE, v0.mat);
}

void ShaderBase::setUniform(int ID, kmMat4 v0)
{
	if (!inUse())
	{
		auto temp = inUseShader_;
		use();
		glUniformMatrix4fv(ID, 1, GL_FALSE, v0.mat);
		temp->use();
	}
	else
		glUniformMatrix4fv(ID, 1, GL_FALSE, v0.mat);
}
