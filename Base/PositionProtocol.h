#pragma once
#include "Config.h"
#include "macros.h"
#include "../kazMath/kazmath.h"
#include <memory>

class Node;

class PositionProtocol {
	kmMat4 transformMatrix_, worldTransformationMatrix_;
	bool transformDirty_;
	PositionProtocol* parent_;
	friend class Node;
public:
	PositionProtocol();
	virtual ~PositionProtocol();	
	void recalculateWorldTransformationMatrix(bool recurse = false);
	const kmMat4& getTransformationMatrix();
	const kmMat4& getWorldTransformationMatrix();
	
	PROPERTY(kmVec3, position_, Position);
	PROPERTY(kmVec3, rotation_, Rotation);
	PROPERTY(kmVec3, scale_, Scale);

	virtual void setPosition(PRECISION x, PRECISION y, PRECISION z);
	virtual void setRotation(PRECISION x, PRECISION y, PRECISION z);
	virtual void setScale(PRECISION x, PRECISION y, PRECISION z);

	PositionProtocol* getParent();
};