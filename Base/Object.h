#pragma once 

class Object 
{
public:
	Object();
	virtual ~Object();
	void retain();
	void release();
private:
	unsigned retainCount_;
};