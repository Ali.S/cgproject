#include "pcHeaders.h"
#include "Shared.h"
#include <iostream>
#include "engine.h"
#include <IL/il.h>
#include <Windows.h>
#include <chrono>

static RendererBase* sharedRenderer;

void display()
{
	static std::chrono::high_resolution_clock::time_point internalTimer = std::chrono::high_resolution_clock::now();
	auto frameBegin = std::chrono::high_resolution_clock::now();
	float dt = std::chrono::duration_cast<std::chrono::microseconds>(frameBegin - internalTimer).count() / 1e6f;
	internalTimer = frameBegin;
	sharedRenderer->display(dt);
	glutSwapBuffers();
	internalTimer = frameBegin;
}

void reshape(int w,int h)
{
	sharedRenderer->reshape(w,h);
}

void keyboard(unsigned char k, int , int)
{
	sharedRenderer->keyboard(k);
}

void mouse(int button,int state,int x,int y)
{
	sharedRenderer->mouse(button,x,y);
}

void timer(int key)
{
	if (key == 0)
	{
		glutTimerFunc(1000/60.f,timer,0);	
		glutPostRedisplay();
	}
}

int main(int argc, char **argv)
{
	glutInitWindowSize(1366, 768);	
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE |GLUT_ALPHA);
	int p;
	for(p = 0;argv[0][p];p++);
	for(;p && argv[0][p] != '\\' && argv[0][p] != '/';--p);
	if(argv[0][p])
		p++;
	glutCreateWindow(argv[0]+p);

	GLenum err = glewInit();
	if (GLEW_OK != err)
	{	  
		std::cerr << "GLEW Error: " <<glewGetErrorString(err) << "\n";
		return 0;
	}
	ilInit();
	int alphaSize;
	glGetIntegerv(GL_ALPHA_BITS, &alphaSize);
	glClearColor(.0, .0, .0, .0);
	sharedRenderer = createRenderer();
	
  
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);	
	glutMouseFunc(mouse);
	glutTimerFunc(0,timer,0);

	ShaderCache::sharedShaderCache();

	glutMainLoop();
	ShaderCache::purgeSharedShaderCache();
	delete []sharedRenderer;
	return 0;
}


int __stdcall WinMain(HINSTANCE hInstance,
				HINSTANCE hPrevInstance,
				LPTSTR    lpCmdLine,
				int       nCmdShow)
{
	return main(1,&lpCmdLine);

}