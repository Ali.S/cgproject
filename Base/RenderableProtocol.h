#pragma once

#include "PositionProtocol.h"

class RenderableProtocol : public PositionProtocol
{
	
public:
	virtual void preRender();
	virtual void render() = 0;
	virtual void postRender() = 0;
	PROPERTY(bool, visible_, Visible);
};

