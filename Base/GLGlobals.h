#pragma once
#include "Object.h"
class Texture;

class GLGlobals
{
public :
	GLGlobals();
	~GLGlobals();
	static GLGlobals* sharedGLGlobals();
	void setArrayBuffer(int bufferID);
	void setElementArrayBuffer(int bufferID);
	void setTransformFeedbackBuffer(int bufferID);
	void setVertexAttributeEnabled(int id, bool status);
	void bindTexture(Texture* texture, int textureUnit = 0);
	void useShader(Object* shader);
	void setRenderTarget(Object* renderTarget);

	Object* getInUseShader();
	Object* getRenderTarget();
private:
	static GLGlobals* sharedGlobals_;

	Object *inUseShader_, *renderTarget_;
	int arrayBuffer_, elementArrayBuffer_, transformFeedbackBuffer_;
	long long vertexAttributeEnabledStatus_;
	Texture** textureIDCache_;
	int textureUnitCount_;
	int activeTextureUnit;
};