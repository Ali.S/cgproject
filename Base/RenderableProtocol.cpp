#include "pcHeaders.h"
#include "RenderableProtocol.h"

void RenderableProtocol::preRender()
{
	recalculateWorldTransformationMatrix();
}

void RenderableProtocol::setVisible(bool value)
{
	visible_ = value;
}

bool RenderableProtocol::getVisible()
{
	return visible_;
}