#pragma once
#include <GL/gl.h>
#include "../KazMath/kazmath.h"
#include "Object.h"

class ShaderBase : public Object
{
public:
	virtual void use();
	virtual bool inUse();
	virtual bool isUsable();
	static ShaderBase* getInUseShader();
	int getUniformID(const char* name);
	int getAttribitueID(const char* name);
	void setUniform(int ID, float v0);
	void setUniform(int ID, float v0, float v1);
	void setUniform(int ID, float v0, float v1, float v2);
	void setUniform(int ID, float v0, float v1, float v2, float v3);
	void setUniform(int ID, int v0);
	void setUniform(int ID, int v0, int v1);
	void setUniform(int ID, int v0, int v1, int v2);
	void setUniform(int ID, int v0, int v1, int v2, int v3);		
	void setUniform(int ID, kmMat3 v0);
	void setUniform(int ID, kmMat4 v0);
protected:
	ShaderBase();
	~ShaderBase();
	GLuint programID_;
	static ShaderBase* inUseShader_;
	bool compileShader(GLuint& shader, GLenum type, const GLchar* source);
	char* logForOpenGLObject(GLuint object, void(__stdcall *infoFunc)(GLuint, GLenum, GLint*), void(__stdcall *logFunc)(GLuint, GLsizei, GLsizei*, GLchar*));
};