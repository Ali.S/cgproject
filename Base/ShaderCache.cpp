#include "pcHeaders.h"
#include "ShaderCache.h"
#include "DefaultShaders.h"

ShaderCache* ShaderCache::sharedShaderCache_ = nullptr;

ShaderCache::ShaderCache()
{
	loadDefaultShaders();
}

ShaderCache* ShaderCache::sharedShaderCache()
{
	if (!sharedShaderCache_)
		sharedShaderCache_ = new ShaderCache();
	return sharedShaderCache_;
}

void ShaderCache::purgeSharedShaderCache()
{
	delete sharedShaderCache_;
}

void ShaderCache::addShader(std::string name, Shader* shader)
{
	shader->retain();
	shaderLibrary_[name] = shader;
}

void ShaderCache::removeShader(std::string name)
{
	auto shader = shaderLibrary_.find(name);
	if (shader!= shaderLibrary_.end())
	{
		shaderLibrary_.erase(shader);
		shader->second->release();
	}
}

Shader* ShaderCache::getShader(std::string name)
{
	return shaderLibrary_[name];
}

void ShaderCache::loadDefaultShaders()
{
	Shader* temp;
	temp = new Shader(ShaderPositionition_v, ShaderPositionition_f); temp->use();
	addShader (SHADERNAME_POSITION, temp);
	temp = new Shader(ShaderPositionitionUniformColor_v, ShaderPositionitionUniformColor_f);
	addShader (SHADERNAME_POSITION_U_COLOR, temp);
	temp = new Shader(ShaderPositionitionColor_v, ShaderPositionitionColor_f);
	addShader (SHADERNAME_POSITION_COLOR, temp);
	temp = new Shader(ShaderPositionitionColorUniformColor_v, ShaderPositionitionColorUniformColor_f);
	addShader (SHADERNAME_POSITION_COLOR_U_COLOR, temp);
	temp = new Shader(ShaderPositionitionTexture_v, ShaderPositionitionTexture_f);
	addShader (SHADERNAME_POSITION_TEXTURE, temp);
	temp = new Shader(ShaderPositionitionTextureUniformColor_v, ShaderPositionitionTextureUniformColor_f);
	addShader (SHADERNAME_POSITION_TEXTURE_U_COLOR, temp);
	temp = new Shader(ShaderPositionitionTextureColor_v, ShaderPositionitionTextureColor_f);
	addShader (SHADERNAME_POSITION_TEXTURE_COLOR, temp);
	temp = new Shader(ShaderPositionitionTextureColorUniformColor_v, ShaderPositionitionTextureColorUniformColor_f);
	addShader (SHADERNAME_POSITION_TEXTURE_COLOR_U_COLOR, temp);	
}
