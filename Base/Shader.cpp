#include "pcHeaders.h"
#include "Shader.h"
#include <cstdlib>
#include <iostream>

using namespace std;	

Shader::Shader()
{

}

Shader::~Shader()
{
}

Shader::Shader(const char* vertexShader, const char* fragmentShader)
{
	compile(vertexShader, fragmentShader);
}

bool Shader::compile(const char* vertexShader, const char* fragmentShader)
{
	if (programID_ != 0)
		glDeleteProgram(programID_);
	programID_ = glCreateProgram();
	if (!programID_)
	{
		std::cerr << "ERROR: unable to create shader program\n";
		return false;
	}
	
	GLuint vShaderID;
	GLuint fShaderID;
	GLint status = 0;
	
	status |= compileShader(vShaderID, GL_VERTEX_SHADER, vertexShader) ? 1 : 0;
	status |= compileShader(fShaderID, GL_FRAGMENT_SHADER, fragmentShader) ? 2 :0;
	
	if (status != 3)
	{
		std::cerr << "\n";
		glDeleteProgram(programID_);
		if (status & 1)
			glDeleteShader(vShaderID);
		if (status & 2)
			glDeleteShader(fShaderID);
		programID_ = 0;
		return false;
	}
	
	glLinkProgram(programID_);	
	glGetProgramiv(programID_, GL_LINK_STATUS, &status);
	glDeleteShader(vShaderID);
	glDeleteShader(fShaderID);
	if (!status)
	{
		GLchar *src;
		std::cerr << "ERROR: linking shader failed.\n";
		src = logForOpenGLObject(programID_, glGetProgramiv, glGetProgramInfoLog);
        std::cerr << "ERROR: linker output\n" << src;
		free (src);
		programID_ = 0;	
		return false;
	}
	return true;
}
