#include "pcHeaders.h"
#include "DefaultShaders.h"

#define STRINGIFY(X) #X

const char* ShaderPositionitionColor_v = STRINGIFY(
	#version 120									\n
													\n
	uniform mat4 u_Vmat;							\n
	uniform mat4 u_Pmat;							\n
	uniform mat4 u_Mmat;							\n
	attribute vec4 a_Position;						\n
	attribute vec4 a_Color;							\n
	varying vec4 v_Color;							\n
													\n
	void main()										\n
	{												\n
		gl_Position = u_Pmat * u_Vmat * u_Mmat * a_Position;		\n
		v_Color = a_Color;							\n
	}
	);

const char* ShaderPositionitionColor_f = STRINGIFY(
	#version 120									\n
													\n	
	varying vec4 a_Color;							\n
													\n
	void main()										\n
	{												\n
		gl_FragColor = a_Color;						\n
	}
	);