#include "pcHeaders.h"
#include "Texture.h"

Texture::Texture() : Object()
{
	glGenTextures(1, &textureID_);
}

Texture::~Texture()
{
	glDeleteTextures(1, &textureID_);
}

GLuint Texture::getTextureID()
{
	return textureID_;
}