#include "Shaders.h"
#define STRINGIFY(X) #X

const char* lightShader_v = STRINGIFY(
	#version 150									\n
													\n
	uniform mat4 u_Vmat;							\n
	uniform mat4 u_Pmat;							\n
	uniform mat4 u_Mmat;							\n
	in vec4 a_Position;							\n	
	in vec3 a_Normal;								\n
	in vec2 a_TexCOORD;								\n
	out vec2 v_TexCOORD;							\n
	out vec3 v_normal;								\n
	out vec4 v_pos;									\n
													\n
	void main()										\n
	{												\n
		gl_Position = u_Pmat * u_Vmat * u_Mmat * a_Position;		\n		
		v_pos = u_Mmat * a_Position;				\n
		mat3 nMat = mat3(u_Mmat);					\n
		nMat = transpose(inverse(nMat));			\n
		v_normal = a_Normal;					\n
		v_TexCOORD = a_TexCOORD;
	}
	);

const char* lightShader_f = STRINGIFY(
	#version 150									\n
													\n	
	uniform vec4 u_Color;							\n	
	uniform vec3 u_LightPos;						\n
	in vec3 v_normal;								\n
	in vec4 v_pos;									\n
	in vec2 v_TexCOORD;								\n
	out vec4 fColor;\n
													\n
	void main()										\n
	{												\n
		vec4 light = vec4(0.1);						\n
		light += 0.9 * abs(dot(normalize(v_normal),normalize(u_LightPos - v_pos.xyz)));	\n
		fColor.xyz = u_Color.xyz * light.xyz;						\n		
		fColor.a = u_Color.a;
	}
	);