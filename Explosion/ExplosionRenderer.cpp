#include "ExplosionRenderer.h"
#include "Shaders.h"
#include <iostream>
#include "ExplodableMesh.h"

using namespace std;
GLuint tbo;
RendererBase* createRenderer()
{
	return new ExplosionRenderer;
}

ExplosionRenderer::ExplosionRenderer()
{
	scene_ = new Node();
	scene_->retain();

	Mesh::VertexInfo vertices[4];
	Texture2D* floorTexture = TextureCache::sharedTextureCache()->addTexture("floor.jpg");
	floorTexture->setWrapMode(GL_REPEAT, GL_REPEAT);
	vertices[0].x = -static_cast<float>(floorTexture->getWidth()); vertices[0].y = 0; vertices[0].z = -static_cast<float>(floorTexture->getHeight()); vertices[0].u =  0; vertices[0].v =  0;
	vertices[1].x = +static_cast<float>(floorTexture->getWidth()); vertices[1].y = 0; vertices[1].z = -static_cast<float>(floorTexture->getHeight()); vertices[1].u = 30; vertices[1].v =  0;
	vertices[2].x = -static_cast<float>(floorTexture->getWidth()); vertices[2].y = 0; vertices[2].z = +static_cast<float>(floorTexture->getHeight()); vertices[2].u =  0; vertices[2].v = 30;
	vertices[3].x = +static_cast<float>(floorTexture->getWidth()); vertices[3].y = 0; vertices[3].z = +static_cast<float>(floorTexture->getHeight()); vertices[3].u = 30; vertices[3].v = 30;
	short indices[] = {0,2,1,1,2,3};
	Mesh* floor = new Mesh(vertices,4,indices,6);
	floor->setTexture(floorTexture);
	floor->setPosition(0,-0.1,0);
	scene_->addChild(floor);

	camera_ = new Camera;
	camera_->setAsActive();
	(new Node)->addChild(camera_);
	(new Node)->addChild(camera_->getParent());
	dynamic_cast<Object*>(camera_->getParent()->getParent())->retain();
	
	camera_->setPosition(0,0,30);
	camera_->getParent()->setRotation(-M_PI/4,0,0);
	camera_->getParent()->getParent()->setRotation(0,M_PI/3,0);
	camera_->getParent()->getParent()->setPosition(5,1,0);

	Shader* lightShader = new Shader(lightShader_v,lightShader_f);
	lightShader->setUniform(lightShader->getUniformID("u_LightPos"), 0.0f,10.0f,0.0f);

	explosionObject_ = new ExplodableMesh;
	explosionObject_->load("airboat.obj");
	explosionObject_->setScale(1,1,1);	
	explosionObject_->setShader(lightShader);
	explosionObject_->setUniformColor(1,1,1);	
	explosionObject_->prepare();
	explosionObject_->Explode(0,0,0,0);
	explosionObject_->step(0);
	scene_->addChild(explosionObject_);
	simulationRunning_ = false;
	exploded_ = false;

	glEnable(GL_DEPTH_TEST);	
}

void ExplosionRenderer::reshape(int w, int h)
{	
	width_ = w;
	height_ = h;	
	float aspect = static_cast<float>(w/2)/h;	
	camera_->setPerspective(60,aspect, 1e-2, 1e3);	
}

void ExplosionRenderer::mouse(int b, int x,int y)
{

}

void ExplosionRenderer::keyboard(int k)
{
	if (k == 27)
		exit(0);	
	kmVec3 up,front,right,res;
	  res.x = 0;  res.y = 0;  res.z = 0;
	   up.x = 0;   up.y = 1;   up.z = 0;
	front.x = 0;front.y = 0;front.z = 1;
	right.x = 1;right.y = 0;right.z = 0;
	kmMat4 cameraRotation = camera_->getWorldTransformationMatrix();	
	cameraRotation.mat[3] = 
		cameraRotation.mat[7] = 
		cameraRotation.mat[11] = 
		cameraRotation.mat[12] = 
		cameraRotation.mat[13] = 
		cameraRotation.mat[14] = 0;
	cameraRotation.mat[15] = 1;
	if (k == 'q' || k == 'z')
		res = up;
	if (k == 'a' || k == 'd')
		kmVec3Transform(&res,&right,&cameraRotation);
	if (k == 's' || k == 'w')
		kmVec3Transform(&res,&front,&cameraRotation);
	if (k == 'z' || k == 'a' || k == 'w')
		kmVec3Scale(&res,&res, -1);
	else
		kmVec3Scale(&res,&res, 1);
	kmVec3Add(&res,&res,&camera_->getParent()->getParent()->getPosition());
	camera_->getParent()->getParent()->setPosition(res);	
	if (k == 'j')
	{
		kmVec3 p = camera_->getParent()->getParent()->getRotation();
		p.y += 0.02;
		camera_->getParent()->getParent()->setRotation(p);
	}
	if (k == 'l')
	{
		kmVec3 p = camera_->getParent()->getParent()->getRotation();
		p.y -= 0.02;
		camera_->getParent()->getParent()->setRotation(p);
	}
	if (k == 'k')
	{
		kmVec3 p = camera_->getParent()->getRotation();
		p.x += 0.02;
		camera_->getParent()->setRotation(p);
	}
	if (k == 'i')
	{
		kmVec3 p = camera_->getParent()->getRotation();
		p.x -= 0.02;
		camera_->getParent()->setRotation(p);
	}
	if (k == 'r')
	{
		explosionObject_->Explode(0,0,0,0);
		explosionObject_->step(0);
		exploded_ = false;
		simulationRunning_ = false;
	}
	if (k == 'p')
	{
		if(!exploded_)
		{
			k=32;
		}
		exploded_ = true;
		simulationRunning_ = !simulationRunning_;
	}
	if (k == 32)
	{
		explosionObject_->Explode(-4,0,0,10);
		exploded_ = true;
		simulationRunning_ = true;
	}
	
}

void ExplosionRenderer::display(float dt)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	static int k =0;
	//if(!k++)
	if(simulationRunning_)
		explosionObject_->step(dt);
	
	scene_->preRender();
	kmVec3 p = camera_->getParent()->getParent()->getRotation();
	p.y = -p.y;
	camera_->getParent()->getParent()->setRotation(p);
	glViewport(0,0,width_/2,height_);
	scene_->render();	
	p.y = -p.y;
	camera_->getParent()->getParent()->setRotation(p);
	glViewport(width_/2,0,width_/2,height_);
	scene_->render();
}