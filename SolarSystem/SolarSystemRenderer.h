#include <Shared.h>
#include <Engine.h>

class SolarSystemRenderer : public RendererBase
{
	float time;
	
	struct planetData{
		float rot_speed;
		float rot_off;
		float spin;
		Node* node;
	};
	std::vector<planetData> planets_;

	struct {
		float x,y,z;
	} campos;
	
	int windowHeight_;
	Mesh* sky_;	
	Node* system_;
	Camera *defaultCamera_, *orthoCamera_;
public :
	SolarSystemRenderer();
	virtual void display(float dt);
	virtual void reshape(int w,int h);
	virtual void keyboard(int k);
	virtual void mouse(int bs, int x,int y);
};

extern const char* solarShader_v;
extern const char* solarShader_f;