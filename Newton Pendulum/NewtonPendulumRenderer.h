#include <Shared.h>
#include <Engine.h>
#include "ExplodableMesh.h"
class Ball;

struct NewtonPendulumRenderer : public RendererBase{
public:
	NewtonPendulumRenderer();
	virtual void display(float dt);
	virtual void reshape(int w,int h);
	virtual void keyboard(int k);
	virtual void mouse(int bs, int x,int y);
protected:
	Node* scene_;
	Camera* camera_;	
	Mesh* floor;
	Ball** balls_;
	bool simulationRunning_;
	int width_,height_;
	float accumeT_;
	const int maxBallCount_;
	int ballCount_;
};

RendererBase* createRenderer();
