#include "Ball.h"

Ball::Ball() : Node(), material_(Metal)
{
	Shader* shader = ShaderCache::sharedShaderCache()->getShader("ballShader");
	materialLocation_ = shader->getUniformID("material");
	sphere_ = new Mesh;
	sphere_->setShader(shader);
	sphere_->setAutoSelectShader(false);	
	addChild(sphere_);
	setUpCameras();
	setUpTextures();
}

Ball::Ball(unsigned vSlice, unsigned hSlice) : Node(), material_(Metal)
{
	sphere_ = (new MeshFactory)->createSphere(vSlice, hSlice);
	Shader* shader = ShaderCache::sharedShaderCache()->getShader("ballShader");
	materialLocation_ = shader->getUniformID("material");
	sphere_->setShader(shader);	
	sphere_->setAutoSelectShader(false);
	addChild(sphere_);
	setUpCameras();
	setUpTextures();
	sphere_->setTexture(ballTexture_);
}

Ball::~Ball()
{
	ballTexture_->release();
	renderTarget_->release();
}

void Ball::setUpCameras()
{
	kmVec3 eye,center,up;
	eye.x = eye.y = eye.z = 0;
	up.x = up.z = 0; up.y = -1;
	center.y = 0;
	for(int i=0;i<6;i++)
	{
		cameras_[i] = new Camera;
		cameras_[i]->setPerspective(90,1,0.01,1000);
	}
	for(int i=0;i<4;i++)
	{
		center.x = (i%2) - i/2;
		center.z = (1 -i / 2) - i % 2;
		cameras_[i]->setVeiw(eye,center,up);
	}
	up.z = 1; up.x = up.y = 0;
	center.x = center.z = 0; center.y = 1;
	cameras_[4]->setVeiw(eye,center,up);	
	up.z = -1; up.x = up.y = 0;
	center.x = center.z = 0; center.y = -1;
	cameras_[5]->setVeiw(eye,center,up);
	for(unsigned i=0;i<6;i++)
		addChild(cameras_[i]);
}

void Ball::setUpTextures()
{
	ballTexture_ = new TextureCube;
	renderTarget_ = new RenderTexture();
	renderTarget_->retain();
	ballTexture_->retain();
	GLenum cubeSides[] = 
	{
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
	};
	GLGlobals::sharedGLGlobals()->bindTexture(ballTexture_);
	for(unsigned i=0;i<6;i++)
		glTexImage2D(cubeSides[i],0,GL_RGBA,RMapSize,RMapSize,0,GL_RGBA,GL_FLOAT, nullptr);
	GLGlobals::sharedGLGlobals()->setRenderTarget(renderTarget_);	
	GLuint rb;
	glGenRenderbuffers(1, &rb);
	glBindRenderbuffer(GL_RENDERBUFFER, rb);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, RMapSize, RMapSize);	
	glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rb);
}

void Ball::prerender(Node* scene)
{
	sphere_->setVisible(false);
	GLenum cubeSides[] = 
	{
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y
	};
	GLGlobals::sharedGLGlobals()->setRenderTarget(renderTarget_);	
	GLGlobals::sharedGLGlobals()->bindTexture(ballTexture_);
	scene->preRender();
	for(unsigned i=0;i<6;i++)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, cubeSides[i], ballTexture_->getTextureID(), 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	
		cameras_[i]->setAsActive();		
		scene->render();
	}
	sphere_->setVisible(true);
}

void Ball::reshape(unsigned vSlice, unsigned hSlice)
{
	(new MeshFactory)->assignSphere(sphere_, vSlice,hSlice);
}

void Ball::render()
{
	sphere_->getShader()->setUniform(materialLocation_, material_);
	Node::render();
}

void Ball::nextMaterial()
{
	switch (material_)
	{
	case Ball::Metal: material_ = Mirror;
		break;
	case Ball::Glass: material_ = Metal;
		break;
	case Ball::Mirror: material_ = Glass;
		break;
	default: material_ = Metal;
		break;
	}

}